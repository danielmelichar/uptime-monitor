# Uptime Monitor

[![pipeline status](https://gitlab.com/danielmelichar/uptime-monitor/badges/main/pipeline.svg)](https://gitlab.com/danielmelichar/uptime-monitor/-/commits/main) 

Verify availability of computing services and websites.

## Why?


## What?

## Who?

## How?

## Future work

**General**

- Add maintenance windows to inform users about downtimes
- Send requests from multiple locations 
- See response times for requests
- Notifications of downtime through channels like Email, twitter, telegram, etc.

## Alternatives

- [Unami](https://umami.is/)
- [Atlassian Statuspage](https://www.atlassian.com/software/statuspage)
- [Status.io](https://status.io/)
- [Hyperping](https://hyperping.io/)

## Author

Daniel Melichar ([daniel@melichar.xyz](mailto:daniel@melichar.xyz))

## License

[MIT](https://choosealicense.com/licenses/mit/)

